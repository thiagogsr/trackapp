function toast(msg) {
  blackberry.ui.toast.show(msg);
}

function getImage(situacao) {
  var imagem = "";
  if(situacao) {
    switch (situacao.toUpperCase().trim()) {
      case "SAIU PARA ENTREGA":
        imagem = "saiu";
        break;
      case "CONFERIDO":
        imagem = "conferido";
        break;
      case "ENCAMINHADO":
        imagem = "encaminhado";
        break;
      case "ENTREGUE":
        imagem = "entregue";
        break;
      case "ESTORNADO":
      case "DEVOLVIDO AO REMETENTE":
        imagem = "estornado";
        break;
      case "SAÍDA CANCELADA":
      case "RECUSADO":
        imagem = "cancel";
        break;
      case "EXTRAVIADO":
      case "DESTINATÁRIO AUSENTE":
        imagem = "lost";
        break;
      case "AGUARDANDO RETIRADA":
      case "POSTADO - DH":
        imagem = "clock";
      default:
        imagem = "postado";
    }
  } else {
    imagem = "clock";
  }
  return "local:///images/icons/" + imagem.toLowerCase() + ".png"
}

function showLoad() {
  document.getElementById('panel').style.display = "none";
  document.getElementById('indicator').style.display = "block";
}

function hideLoad(showForm) {
  document.getElementById('indicator').style.display = "none";
  if(showForm) document.getElementById('panel').style.display = "block";
}

function normalizeInputs() {
  var codigo = document.getElementById("codigo");
  var descricao = document.getElementById("descricao");
  codigo.onkeyup = function(e) {
    codigo.value = codigo.value.toUpperCase();
  }
  descricao.onkeyup = function(e) {
    descricao.value = descricao.value.toCapitalize();
  }
}

String.prototype.trim = function(){
  return this.replace(/^\s+|\s+$/g, '');
};

String.prototype.fulltrim = function() {
  return this.replace(' ', '');
};

String.prototype.toCapitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}