//CREATE DB
var db = openDatabase(
  'trackapp',
  '1.0',
  'Banco de dados referente ao armazenamento mobile',
  5 * 1024 * 1024
);

function loadTracks(entregue, callback) {
  db.transaction(function (tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS tracks (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, codigo VARCHAR(13) NOT NULL, descricao VARCHAR(50), entregue BOOLEAN, situacao VARCHAR(50))', [], function(tx) {
      tx.executeSql('CREATE TABLE IF NOT EXISTS track_status (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, track_id INTEGER NOT NULL, detalhes VARCHAR(200), local VARCHAR(200), data VARCHAR(16), situacao VARCHAR(50))', [], function(tx) {
        tx.executeSql('CREATE INDEX IF NOT EXISTS idx_track_id ON track_status (track_id)', [], function(tx) {
          tx.executeSql('SELECT t.id, t.codigo, t.descricao, t.situacao AS imagem, t.entregue, CASE WHEN ts.detalhes = "" THEN t.situacao ELSE ts.detalhes END AS situacao, ts.data FROM tracks t LEFT JOIN track_status ts ON t.id = ts.track_id WHERE t.entregue = ' + entregue + ' GROUP BY t.id ORDER BY t.entregue ASC, t.id DESC', [], function (tx, results) {
            if(addItens(results, entregue, callback) === 0) {
              toast("Nenhuma encomenda encontrada.");
            }
          });
        });
      });
    });
  });
}

function addItens(results, entregue, callback) {
  var dataList = document.getElementById(entregue === 2 ? "waitList" : "dataList");
  document.getElementById("dataList").innerHTML = "";
  document.getElementById("waitList").innerHTML = "";

  var count = results.rows.length,
      itens = [],
      category,
      header = document.createElement('div');

  header.setAttribute('data-bb-type', 'header');
  switch(entregue) {
    case 0:
      category = 'A caminho';
      break;
    case 1:
      category = 'Entregues';
      break;
    case 2:
      category = 'Aguardando';
      break;
  }
  header.innerHTML = category;
  itens.push(header);

  if (count > 0) {
    for (var i = 0; i < count; i++) {
      var track = results.rows.item(i),
          item = document.createElement('div'),
          imagem = getImage(track.imagem),
          noDetails = track.situacao ? false : true;

      item.setAttribute('data-bb-type', 'item');
      item.setAttribute('data-bb-img', imagem);
      item.setAttribute('data-bb-title', track.descricao);
      if(entregue === 2) {
        item.innerHTML = "Aguardando atualização";
      } else {
        item.innerHTML = track.data.substr(0, 5) + track.data.subtr(9, 5) + " " + track.situacao;
      }
      item.setAttribute('id', track.id);
      if(entregue !== 2) {
        item.onclick = function() {
          bb.pushScreen("showTrack.html", "show", dataList.selected);
        }
      }
      itens.push(item);
    }
  }  

  dataList.refresh(itens);

  if (bb.scroller) {
    bb.scroller.refresh();
  }

  if(callback) {
    callback();
  }

  return count;
}

function loadDetails(track, callback) {
  db.transaction(function (tx) {
    tx.executeSql('SELECT t.id, t.codigo, t.descricao, ts.situacao AS imagem, ts.situacao, CASE WHEN ts.detalhes = "" THEN ts.local ELSE ts.detalhes END AS local, ts.data, ts.detalhes FROM track_status ts LEFT JOIN tracks t ON ts.track_id = t.id WHERE t.id = ? ORDER BY ts.id DESC', [track.id], function (tx, results) {
      var count = results.rows.length;
      if (count > 0) {
        var dataList = document.getElementById('dataList'),
            itens = [],
            codigo = results.rows.item(0).codigo,
            descricao = results.rows.item(0).descricao;

        document.getElementById("titulo").setCaption(descricao);

        var header = document.createElement('div');
        header.setAttribute('data-bb-type', 'header');
        header.innerHTML = codigo;
        itens.push(header);

        document.getElementById('btUpdate').onclick = function() {
          updateStatus(track.id, codigo, function() {
            loadDetails(track, function() {
              toast("Encomenda atualizada.");
            });
          });
        }

        document.getElementById('btEdit').onclick = function() {
          bb.pushScreen('editTrack.html','edit', track);
        }

        document.getElementById('btDelete').onclick = function() {
          removeTrack(track, true);
        }

        for (var i = 0; i < count; i++) {
          var detail = results.rows.item(i),
              item = document.createElement('div'),
              imagem = getImage(detail.imagem);

          item.setAttribute('data-bb-type', 'item');
          item.setAttribute('data-bb-img', imagem);
          item.setAttribute('data-bb-title', detail.situacao);
          item.setAttribute('data-bb-accent-text', detail.data);
          item.innerHTML = detail.local;
          itens.push(item);
        }
        
        dataList.refresh(itens);

        if (bb.scroller) {
          bb.scroller.refresh();
        }

        if(callback) {
          callback();
        }
      }
    });
  });
}

// metodo que busca as atualizacoes no servidor
function getStatus(codigo, tracknumber, callback) {
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if (request.readyState == 4) {
      var newStatus = false;
      var entregue = 0;
      try {
        var response = request.responseXML;
        var eventos = response.getElementsByTagName("evento");
        var count = eventos.length;

        db.transaction(function (tx) {
          tx.executeSql("DELETE FROM track_status WHERE track_id = ?", [codigo], function(tx) {
            if(count > 0) {
              tx.executeSql("UPDATE tracks SET entregue = 0 WHERE id = ?", [codigo], function(tx) {
                var lastDate = lastStatus = undefined;
                for(var i = (count - 1); i >= 0; i--) {
                  var tags = eventos[i].childNodes,
                      status = [],
                      detalhes = "";
                  
                  for (var c = 1; c < tags.length; c += 2) {
                    var el = tags[c],
                        key = el.nodeName;
                    
                    if(key) {
                      if(key === "destino") {
                        var destino = el.childNodes;
                        status[key] = destino[1].textContent;
                      } else {
                        var valor = el.textContent;
                        if(valor) {
                          status[key] = valor;
                        }
                      }
                    }
                  }

                  if(status["destino"]) {
                    detalhes = "Em trânsito para " + status["destino"];
                  }

                  var data_hora = status["data"] + " " + status["hora"],
                      descricao = status["descricao"],
                      itemData = [codigo, detalhes, status["local"], data_hora, descricao];

                  tx.executeSql("INSERT INTO track_status (track_id, detalhes, local, data, situacao) VALUES (?, ?, ?, ?, ?)", itemData, function(tx) {
                    if(descricao.toUpperCase().trim() === "ENTREGUE") {
                      tx.executeSql("UPDATE tracks SET entregue = 1 WHERE id = ?", [codigo]);
                      entregue = 1;
                    }  
                  });

                  if(i === 0) {
                    lastDate = data_hora;
                    lastStatus = descricao;
                  }
                }
                tx.executeSql("SELECT situacao FROM tracks WHERE id = ?", [codigo], function(tx, results) {
                  if(results.rows.item(0).data !== data_hora) {
                    newStatus = true;
                    tx.executeSql('UPDATE tracks SET situacao = ? WHERE id = ?', [lastStatus, codigo]);
                  }
                });
              });
            }
          });
        });
      } catch(e) {
        console.log("Error: " + e);
      }

      if(callback) {
        callback(newStatus, entregue);
      }
    }
  }

  request.open("GET", "http://websro.correios.com.br/sro_bin/sroii_xml.eventos?usuario=ECT&senha=SRO&tipo=L&resultado=T&objetos=" + tracknumber);
  request.overrideMimeType("text/xml");
  request.send();
}

// metodo para buscar novas atualizacoes dentro da aplicacao
function updateStatus(id, tracknumber, callback) {
  toast("Atualização iniciada...");
  getStatus(id, tracknumber, callback);
}

function updateTrackStatus(track) {
  db.transaction(function(tx) {
    tx.executeSql("SELECT id, codigo FROM tracks WHERE id = ?", [track.id], function(tx, results) {
      var track = results.rows.item(0);
      updateStatus(track.id, track.codigo, function(newStatus, entregue) {
        loadTracks(entregue, function() {
          toast("Encomenda atualizada.");
        });
      });
    });
  });
}

// metodo para buscar novas atualizacoes para o hub
function searchStatus(id, tracknumber) {
  getStatus(id, tracknumber, function(newStatus, entregue) {
    if(newStatus) {
      db.transaction(function(tx) {
        tx.executeSql("SELECT t.descricao, t.situacao, ts.data FROM tracks t LEFT JOIN track_status ts ON t.id = ts.track_id WHERE t.id = ? GROUP BY t.id", [item.id], function(tx, results) {
          var notify = localStorage.getItem('notify');
          if(notify) {
            var track = results.rows.item(0);
            var title = "TrackApp";
            var options = {
              body: "Sua encomenda '" + track.descricao + "' (" + track.codigo + ") recebeu uma atualização: " + track.situacao + " em " + track.data,
            };
            new Notification(title, options);
          }
        });
      })
    }
  });
}

function cronJob() {
  db.transaction(function(tx) {
    tx.executeSql("SELECT id, codigo FROM tracks WHERE entregue IN (0, 2)", [], function(tx, results) {
      for (var i = 0; i < results.rows.length; i++) {
        var track = results.rows.item(i);
        searchStatus(track.id, track.codigo);
      }
    });
  });

  window.setTimeout("cronJob()", 1800 + 1000); // a cada 30 minutos
}

cronJob();

function editTrack(track) {
  db.transaction(function (tx) {
    tx.executeSql("SELECT id, codigo, descricao FROM tracks WHERE id = ?", [track.id], function(tx, results) {
      document.getElementById('id').value = results.rows.item(0).id;
      document.getElementById('codigo').value = results.rows.item(0).codigo;
      document.getElementById('descricao').value = results.rows.item(0).descricao;
    });
  });
}

function addTrack() {
  showLoad();

  var codigo = document.getElementById('codigo').value.fulltrim().toUpperCase(),
      descricao = document.getElementById('descricao').value.trim().toCapitalize();

  db.transaction(function(tx) {
    tx.executeSql("SELECT COUNT(*) AS total FROM tracks WHERE codigo = ?", [codigo], function(tx, results) {
      if(results.rows.item(0).total === 0) {        
        if (codigo.length != 13 && !codigo.match(/[A-Z]{2}[0-9]{9}[A-Z]{2}/g)) {
          hideLoad(true);
          toast("Informe um código válido.\nEx: PD024339454BR.");
          return;
        }

        if(descricao == "") {
          hideLoad(true);
          toast("Informe uma descrição para a encomenda.");
          return;
        }

        db.transaction(function (tx) {
          tx.executeSql("INSERT INTO tracks (codigo, descricao, entregue) VALUES (?, ?, ?)", [codigo, descricao, 2], function(tx, results) {
            getStatus(results.insertId, codigo, function(newStatus, entregue) {
              hideLoad(false);
              toast("Encomenda adicionada com sucesso.");
              bb.popScreen();
            });
          }, function (tx, error) {
            hideLoad(true);
            alert('Ops... ' + error.message);
          });  
        });
      } else {
        hideLoad(true);
        toast("Encomenda já cadastrada.");
        return;
      }
    })
  });
  
}

function updateTrack() {
  showLoad();
  var codigo = parseInt(document.getElementById('id').value),
      descricao = document.getElementById('descricao').value.trim().toCapitalize();

  if(descricao == "") {
    hideLoad(true);
    toast("Informe uma descrição para a encomenda.");
    return;
  }
 
  db.transaction(function (tx) {
    tx.executeSql("UPDATE tracks SET descricao = ? WHERE id = ?", [descricao, codigo], function() {
      hideLoad(false);
      toast("Encomenda atualizada com sucesso."); 
      bb.popScreen();
    });
  });
}

function removeTrack(item, details) {
  var codigo = item.id;
  blackberry.ui.dialog.customAskAsync("Isso irá excluir o item selecionado", ["Cancelar", "Excluir"], function(idx) {
    if(idx === 1) {
      db.transaction(function (tx) {
        tx.executeSql("DELETE FROM track_status WHERE track_id = ?", [codigo], function(tx) {
          tx.executeSql("DELETE FROM tracks WHERE id = ?", [codigo], function() {
            if(details) {
              bb.popScreen();
            } else {
              item.remove();  
            }
            toast("Encomenda excluída com sucesso.");
          });
        });
      });
    }
  }, {title: "Excluir?"});
}

function loadSettings() {
  db.transaction(function (tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS config (notify BOOLEAN)', [], function(tx) {
     tx.executeSql('SELECT * FROM config', [], function(tx, results) {
      var notify;
      if(results.rows.length === 0) {
        notify = 0;
        tx.executeSql('INSERT INTO config (notify) VALUES (?)', [notify]);
      } else {
        notify = results.rows.item(0).notify;
      }
      var boolNotify = notify === 1 ? true : false;
      localStorage.setItem('notify', boolNotify);
      document.getElementById('notifyEnabled').setChecked(boolNotify);
     }); 
    });
  });
}

function saveSettings() {
  var notify = document.getElementById('notifyEnabled').getChecked();
  var numNotify = notify ? 1 : 0;
  localStorage.setItem('notify', notify);
  db.transaction(function (tx) {
    tx.executeSql('UPDATE config SET notify = ?', [numNotify]);
  });
}